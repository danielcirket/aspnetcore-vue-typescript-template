﻿const path = require('path');

module.exports = {
    configureWebpack: {
        module: {
            rules: [
                {test: /\.html$/, loader: 'raw-loader', exclude: ['index.html']}
            ]
        },
        resolve: {
            symlinks: true,
            alias: {
                '@': path.join(__dirname, 'src'),
                vue$: 'vue/dist/vue.esm.js'
            }
        }
    }
}