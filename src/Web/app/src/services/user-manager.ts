import { UserManager, User } from 'oidc-client';
import configuration from '@/configuration/oidc-configuration';
import Store from '@/stores/store';
import { UserMutationConstants } from '@/stores/user/mutations';
import { UserActionConstants } from '@/stores/user/actions';

const userManager = new UserManager(configuration);

userManager.events.addUserLoaded((user: User) => {
    Store.commit(UserMutationConstants.SETTOKEN, user.access_token);
    Store.commit(UserMutationConstants.SETEMAIL, user.profile.name);
});
userManager.events.addUserSignedOut(() => {
    Store.dispatch(UserActionConstants.LOGOUT);
});
userManager.events.addSilentRenewError((error) => {
    console.log(error);    
});



export default userManager;