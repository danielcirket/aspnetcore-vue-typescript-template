import { AxiosResponse } from 'axios';

export async function DefaultResponseInterceptor(response: AxiosResponse<any>): Promise<AxiosResponse<any>> {
    return response;
}