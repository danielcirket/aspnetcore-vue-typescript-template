import axios from 'axios';
import Router from '@/router';

export async function AuthResponseFailureInterceptor(error: any): Promise<any> {
    if (error.response && error.response.status === 401) {
        if (error.config && !error.config.__isRetryRequest /*&& Store.getters[StoreGetters.REFRESH_TOKEN] != null*/) {
            //await Store.dispatch(StoreActions.REFRESH_LOGIN);
            error.config.__isRetryRequest = true;
            return axios(error.config);
        } else {
            //await Store.dispatch(StoreActions.LOGOUT);

            Router.push('login');
        }
    }

    return error;
}