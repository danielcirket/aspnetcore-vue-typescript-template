import axios from 'axios';
import { AxiosRequestConfig, CancelToken } from 'axios';
import { DefaultResponseInterceptor } from '@/api/interceptors/default-response-interceptor';
import { AuthResponseFailureInterceptor } from '@/api/interceptors/auth-response-failure-interceptor';
import { ApplyAuthenticationHeader } from '@/api/interceptors/auth-request-interceptor';

export abstract class Api {
    private readonly _baseUrl: string = process.env.NODE_ENV == 'production' 
        ? `https://api.${location.host}/` 
        : 'https://localhost:3000/';

    protected async GetAsync<T>(url: string, cancellationToken: CancelToken | null): Promise<T> {
        url = this.BuildUrl(url);
        //this._logger.Debug(`Performing GET request to: '${url}'.`);
        
        try {
            const response = await axios.get(url, this.BuildConfig(cancellationToken));

            this.ThrowIfRequestWasUnsuccessful(url, response);

            return response.data as T;
        } catch (e) {
            throw e;
        }
    }
    protected async PostAsync<T>(url: string, data: any, cancellationToken: CancelToken | null): Promise<T> {
        url = this.BuildUrl(url);
        //this._logger.Debug(`Performing POST request to: '${url}' with data: .`, JSON.stringify(data));

        try {
            const response = await axios.post(url, data, this.BuildConfig(cancellationToken));

            this.ThrowIfRequestWasUnsuccessful(url, response);

            return response.data as T;
        } catch (e) {
            throw e;
        }
    }
    protected async PutAsync<T>(url: string, data: any, cancellationToken: CancelToken | null): Promise<T> {
        url = this.BuildUrl(url);
        //this._logger.Debug(`Performing PUT request to: '${url}' with data: .`, JSON.stringify(data));

        try {
            const response = await axios.put(url, data, this.BuildConfig(cancellationToken));

            this.ThrowIfRequestWasUnsuccessful(url, response);

            return response.data as T;
        } catch (e) {
            throw e;
        }
    }
    protected async PatchAsync<T>(url: string, data: any, cancellationToken: CancelToken | null): Promise<T> {
        url = this.BuildUrl(url);
        //this._logger.Debug(`Performing PATCH request to: '${url}' with data: .`, JSON.stringify(data));

        try {
            const response = await axios.patch(url, data, this.BuildConfig(cancellationToken));

            this.ThrowIfRequestWasUnsuccessful(url, response);

            return response.data as T;
        } catch (e) {
            throw e;
        }
    }
    protected async DeleteAsync<T>(url: string, cancellationToken: CancelToken | null): Promise<T> {
        url = this.BuildUrl(url);
        //this._logger.Debug(`Performing DELETE request to: '${url}'.`);
        try {
            const response = await axios.delete(url, this.BuildConfig(cancellationToken));
            this.ThrowIfRequestWasUnsuccessful(url, response);
            return response.data as T;
        } catch (e) {
            throw e;
        }
    }

    private BuildUrl(url: string): string {
        if (url == null) {
            throw Error('\'url\' cannot be null');
        }

        // TODO(Dan): Do something better with this!
        if (url.length < 1) {
            return this._baseUrl;
        }

        let result: string = this._baseUrl;

        if (result[result.length - 1] !== '/' && url[0] !== '/') {
            result += '/';
        }

        if (result[result.length - 1] === '/' && url[0] === '/') {
            result = result.slice(0, -1);
        }

        return result += url;
    }
    private BuildConfig(cancellationToken: CancelToken | null): AxiosRequestConfig {
        const config: AxiosRequestConfig = {
            cancelToken: cancellationToken || undefined,
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json',
            },
            validateStatus: (status: number) => {
                return status >= 200 && status < 300;
            },

        };

        return config;
    }
    private ThrowIfRequestWasUnsuccessful(url: string, response: any) {
        if (response && response.response && ( response.response.status < 200 ||  response.response.status >= 300)) {
            throw new Error(`Request to ${url} failed with a response status of: ${response.status}. ` + JSON.stringify(response.response));
        }
    }

    protected constructor() {
        axios.interceptors.request.use(ApplyAuthenticationHeader);
        axios.interceptors.response.use(DefaultResponseInterceptor, AuthResponseFailureInterceptor);
    }
}