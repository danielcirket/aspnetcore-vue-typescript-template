import Vue from 'vue';
import Vuex, { ActionContext } from 'vuex';
import { UserStore } from '@/stores/user/store';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        user: new UserStore(),
    },
    getters: {
        IsLoading: (state) => state.IsLoading,
    },
    state: {
        IsLoading: true,
    },
    actions: {
        ShowLoading: (context: ActionContext<any, any>, loading: boolean) => {
           context.commit('SetLoading', loading);
        },
    },
    mutations: {
        SetLoading: (state: any, loading: boolean) => {
            state.IsLoading = loading;
        },
    },
});
