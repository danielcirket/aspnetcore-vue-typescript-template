import { MutationTree } from 'vuex';
import { State } from '@/stores/user/state';

export class Mutations implements MutationTree<State> {
    [key: string]: (state: State, payload: any) => any;

    public SetEmail = (state: State, email: string | null) => {
        state.Email = email;
    }
    public SetToken = (state: State, token: string | null) => {
        state.Token = token;
    }
}

export class UserMutationConstants {
    public static readonly SETEMAIL: string = `user/SetEmail`;
    public static readonly SETTOKEN: string = `user/SetToken`;
}
