import manager from '@/services/user-manager';
import configuration from '@/configuration/oidc-configuration';

const state = sessionStorage.getItem(`oidc.user:${configuration.authority}:staffsquared-web-client`);
const parsed = state == null ? null : JSON.parse(state) as any;

export class State {
    public Email: string | null = parsed == null ? null : parsed.profile.name;
    public Token: string | null = null;
}
