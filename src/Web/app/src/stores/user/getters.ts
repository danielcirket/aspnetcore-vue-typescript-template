import { GetterTree } from 'vuex';
import { State } from '@/stores/user/state';

export class Getters implements GetterTree<State, any> {
    [key: string]: (state: State, getters: any, rootState: any, rootGetters: any) => any;

    public Email = (state: State) => state.Email;
    public Token = (state: State) => state.Token;
}

export class UserGetterConstants {
    public static readonly EMAIL: string = `user/Email`;
    public static readonly TOKEN: string = `user/Token`;
}
