import { ActionTree, ActionContext } from 'vuex';
import { State } from '@/stores/user/state';
import { UserMutationConstants } from '@/stores/user/mutations';
import userManager from '@/services/user-manager';

export class Actions implements ActionTree<State, any> {
    [key: string]: ((injectee: ActionContext<State, any>, payload: any) => any);
    
    public IsLoggedIn = async (context: ActionContext<State, any>) : Promise<boolean> => {
        const user = await userManager.getUser();

        if (user != null) {
            context.commit(UserMutationConstants.SETEMAIL, user.profile.name, { root: true });
        }

        return user != null;
    }
    public Logout =  async (context: ActionContext<State, any>) => {
        context.commit(UserMutationConstants.SETTOKEN, null, { root: true });
        await userManager.signinRedirect();
    }
    public SigninRedirect = async (context: ActionContext<State, any>) => {
        await userManager.signinRedirect();
    }
    public SigninRedirectCallback = async (context: ActionContext<State, any>) => {
        try {
            const user = await userManager.signinRedirectCallback();
            context.commit(UserMutationConstants.SETTOKEN, user.access_token, {root: true});
            context.commit(UserMutationConstants.SETEMAIL, user.profile.name, {root: true});
            window.location.replace(window.location.origin);
        } catch(e) {
            window.location.replace(window.location.origin);
        }
    }
    public SigninSilentCallback = async (context: ActionContext<State, any>) => {
        try {
            await userManager.signinSilentCallback();
        } catch(e) {
            window.location.replace(window.location.origin);
        }
    }
}

export class UserActionConstants {
    public static readonly SIGNIN_WITH_REDIRECT: string = `user/SigninRedirect`;
    public static readonly SIGNIN_REDIRECT_CALLBACK: string = `user/SigninRedirectCallback`;
    //public static readonly SIGNIN_REFRESH: string = `user/SigninSilent`;
    public static readonly SIGNIN_REFRESH_CALLBACK: string = `user/SigninSilentCallback`;
    public static readonly LOGOUT: string = `user/Logout`;
    public static readonly CHECK_LOGGED_IN: string = `user/IsLoggedIn`;
}
