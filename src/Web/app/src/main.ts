﻿import Vue from 'vue';
import app from '@/app.vue';
import router from '@/router';
import store from '@/stores/store';
import Buefy from 'buefy';

Vue.use(Buefy);
Vue.config.productionTip = false;

const root = new Vue({
  router,
  store,
  el: '#app',
  render: (h) => h(app),
});