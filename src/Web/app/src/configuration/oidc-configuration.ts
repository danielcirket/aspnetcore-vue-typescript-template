import { UserManagerSettings, Log } from 'oidc-client';

Log.logger = console;
Log.level = Log.DEBUG;

const settings: UserManagerSettings = {
    authority: 'https://localhost:44355',
    client_id: 'staffsquared-web-client',
    redirect_uri: window.location.origin + '/signed-in',
    response_type: 'id_token token',
    scope: 'openid profile api',
    post_logout_redirect_uri: window.location.origin + '/',
    automaticSilentRenew: true,
    silent_redirect_uri: window.location.origin + '/sign-in-refresh',
    revokeAccessTokenOnSignout: true,
    // client_secret: '141CF4ED2C03DC620AC12ECDCF6A374AB89E441B3ECB24D6E6FD1204CC0FF909',
};

export default settings;
