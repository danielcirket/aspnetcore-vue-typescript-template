import Vue from 'vue';
import Router, { RouterOptions } from 'vue-router';

import Store from '@/stores/store';

import Dashboard from '@/pages/dashboard/dashboard.vue';


Vue.use(Router);

const routerOptions: RouterOptions = {
  mode: 'history',
  routes: [
    
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        title: 'Dashboard',
        requiresAuthentication: true,
      },
    },
  ],
};

const router = new Router(routerOptions);

router.afterEach((to, from) => {
  document.title = `${to.meta.title} - Atlas Barcode`;
});
router.beforeEach(async (to, from, next) => {
  // const isLoggedIn = await Store.dispatch(UserActionConstants.CHECK_LOGGED_IN);
  
  // if (!isLoggedIn && to.meta.requiresAuthentication) {
  //   await Store.dispatch(UserActionConstants.SIGNIN_WITH_REDIRECT);
  //   return;
  // }

  next();
  Store.dispatch('ShowLoading', false);
});

export default router;
